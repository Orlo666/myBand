Rails.application.routes.draw do
  get 'invite/:link_id', to: 'band_members#invite'
  get 'bands/:id/delete', to: 'bands#delete', as: 'delete_band'
  get 'bands/:band_id/albums/:id/delete', to: 'albums#delete', as: 'delete_band_album'
  get 'bands/:band_id/band_members/:id/delete', to: 'band_members#delete', as: 'delete_band_band_member'
  get "/takedown-guidance", to: "welcome#notification", as: "takedown_guidance"
  resources :users, only: [:edit, :update, :show]
  resources :bands do
    collection do
      match 'search' => 'bands#search', via: [:get, :post], as: :search
    end
    resources :band_members, only: [:create, :update, :destroy, :edit]
    resources :albums, only: [:new, :create, :destroy, :edit, :update] do
      resources :songs, only: [:new, :update, :edit, :create, :destroy]
    end
    resources :user_reviews, only: [:create, :update, :destroy]
    resources :events, only: [:show, :new, :update, :edit, :create, :destroy]
  end
  root 'welcome#index'
  get "/auth/:provider/callback" => 'sessions#create'
  resources :sessions, only: [:create, :destroy]
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
end
