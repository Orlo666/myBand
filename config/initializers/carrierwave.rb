CarrierWave.configure do |config|
  config.dropbox_app_key = ENV["BOX_APP_KEY"]
  config.dropbox_app_secret = ENV["BOX_APP_SECRET"]
  config.dropbox_access_token = ENV["BOX_ACCESS_TOKEN"]
  config.dropbox_access_token_secret = ENV["BOX_ACCESS_TOKEN_SECRET"]
  config.dropbox_user_id = ENV["BOX_USER_ID"]
  config.dropbox_access_type = "app_folder"
end
