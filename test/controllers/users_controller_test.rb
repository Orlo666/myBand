require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get after_login" do
    get users_after_login_url
    assert_response :success
  end

end
