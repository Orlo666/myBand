require 'test_helper'

class BandMembersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get band_members_new_url
    assert_response :success
  end

  test "should get edit" do
    get band_members_edit_url
    assert_response :success
  end

  test "should get delete" do
    get band_members_delete_url
    assert_response :success
  end

  test "should get show" do
    get band_members_show_url
    assert_response :success
  end

end
