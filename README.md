# README

# MyBand

School project for creating new bands

You need ruby 2.0+ and rails 5.0+

## Installation

```
bundle install
```

```
rails db:migrate
```

```
rails db:seed
```

```
rails server
```

## Usage

For using oauth please set environment variables CLIENT_SECRET and CLIENT_ID

Enjoy :)
