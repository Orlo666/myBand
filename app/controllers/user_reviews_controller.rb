class UserReviewsController < ApplicationController
  before_action :check_user_logged_in, only: [:create, :update]
  def create
    find_band
    review = UserReview.new(user_reviews_params)
    review.review = params[:review]
    review.band = @band
    review.user = current_user
    if @band.users.include?(current_user)
      review.rating = -1
    end
    if review.save
      unless update_band_rating
        binding.pry
        return
      end
      redirect_to @band
      return
    else
      redirect_to @band, flash: { notice: 'An error occurred, please try it again' }
      return
    end
  end

  def destroy
    find_band
    review = @band.user_reviews.find(params[:id])
    if review.destroy
      update_band_rating
      redirect_to @band
    else
      redirect_to @band, flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def update
    find_band
    review = UserReview.find(params[:id])
    review.review = params[:review]
    if @band.users.include?(current_user)
      if review.save
        redirect_to @band
      else
        render "edit", flash: { notice: 'An error occurred, please try it again' }
      end
      return
    elsif review.update(user_reviews_params)
      update_band_rating
      redirect_to @band
    else
      render "edit", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def user_reviews_params
    if params[:user_review]
      params[:user_review].permit(:rating)
    end
  end

  def update_band_rating
    reviews = @band.user_reviews.where('rating > -1')
    unless reviews.empty?
      number_of_reviews = reviews.length
      sum_rating = 0
      reviews.each do |review|
        sum_rating += review.rating
      end
      @band.rating = sum_rating.to_f / number_of_reviews.to_f
      unless @band.save
        render "edit", flash: { notice: 'An error occurred, please try it again' }
        return false
      end
    end
    return true
  end

  def find_band
    @band = Band.find(params[:band_id])
  end
end
