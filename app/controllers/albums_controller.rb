class AlbumsController < ApplicationController
  before_action :check_user_logged_in, only: [:edit, :new, :create, :update]
  before_action only: [:edit, :create, :delete, :destroy, :new, :update] do
    check_user_got_right(@band)
  end
  def new
    find_band
    @album = @band.albums.new
  end

  def create
    find_band
    @album = @band.albums.create(album_params)
    if @album.save
      redirect_to @band
    else
      render "new", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def delete
    find_band
    @album = Album.find(params[:id])
  end

  def destroy
    find_band
    @album = Album.find(params[:id])
    @album.songs.destroy_all
    @album.destroy
    redirect_to @band
  end

  def album_params
    params.require(:album).permit(:name, :pict)
  end

  def edit
    find_band
    @album = @band.albums.find(params[:id])
  end

  def update
    find_band
    @album = @band.albums.find(params[:id])
    unless @album.update(album_params)
      render "edit", flash: { notice: 'An error occurred, please try it again' }
      return
    end
    items_order = []
    unless params[:items_order].empty?
      items_order = JSON.parse(params[:items_order])
    end
    pos = 0
    items_order.each do |item|
      song = Song.find(item)
      song.priority = pos
      if song.save
        pos += 1
      else
        render "edit", flash: { notice: 'An error occurred, please try it again' }
        return
      end
    end
    redirect_to @band
  end

  def find_band
    @band = Band.find(params[:band_id])
  end
end
