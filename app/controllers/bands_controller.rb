require 'digest/sha1'
require 'pry'
class BandsController < ApplicationController
  before_action :check_user_logged_in, only: [:edit, :new, :create, :update]
  before_action only: [:edit, :update, :delete, :destroy] do
    check_user_got_right(@band)
  end

  def index
    @bands = @q.result.includes(:band_members, :albums)
  end

  def new
    @band = Band.new
    @band_member = BandMember.new
  end

  def search
    index
    render :index
  end

  def edit
    @band = Band.find(params[:id])
  end

  def update
    @band = Band.find(params[:id])
    @band.update(band_params)
    if @band.save

      redirect_to @band
    else
      redirect_to edit_band_path(@band), flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def create
    @band = Band.new(band_params)
    instruments = params[:band][:band_members][:instruments].select { |value| value != "" }
    another_instrument = params[:band][:band_members][:another_instrument]
    if another_instrument == "" && instruments.empty?
      @instruments_empty = true
      render "new", flash: { notice: 'An error occurred, please try it again' }
      return
    else
      @instruments_empty = false
    end
    @band.rating = 3
    if @band.save
      link = Digest::SHA1.hexdigest [@band.id, rand].join
      @band_member = @band.band_members.new(band_member_params)
      @band_member.user = current_user
      instruments.each do |instrument|
        @band_member.instruments << Instrument.find(instrument)
      end
      @album = @band.albums.new(name: 'Basic')
      if @band_member.nickname == ""
        @band_member.nickname = current_user.name
      end
      if @band_member.save && @album.save && @band.update(:link => link)
        redirect_to @band
        return
      else
        @band.destroy
        render "new", flash: { notice: 'An error occurred, please try it again' }
        return
      end
    else
      render "new", flash: { notice: 'An error occurred, please try it again' }
      return
    end
  end

  def band_params
    params.require(:band).permit(:name, :description, :logo)
  end

  def band_member_params
    params[:band][:band_members].permit(:nickname, :instruments, :another_instrument)
  end

  def show
    @band = Band.find(params[:id])
    @band_members = @band.band_members
    @albums = @band.albums
    @reviews = @band.user_reviews.order('created_at DESC')
    unless current_user.nil?
      @new_review = UserReview.new(user: current_user)
    end
    @events = @band.events.order('date')
    @events.each do |event|
      if event.date < Date.yesterday
        event.destroy
      end
    end
    if @band.users.include?(current_user)
      @link = request.base_url + '/invite/' + @band.link
      render 'show_admin.html.slim'
    else
      @band.link = ""
    end
  end

  def delete
    @band = Band.find(params[:id])
  end

  def destroy
    @band = Band.find(params[:id])
    albums = @band.albums
    albums.destroy_all
    @band.events.destroy_all
    @band.band_members.map(&:destroy)
    @band.user_reviews.destroy_all
    @band.destroy
    redirect_to root_path
  end
end
