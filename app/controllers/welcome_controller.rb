require 'pry'
class WelcomeController < ApplicationController
  def index
    if current_user
      @user_bands = current_user.bands
    end
    @top_bands = Band.order('rating DESC').take(5)
    @newest_bands = Band.order('created_at DESC').take(5)
    @q = Band.ransack(params[:q])
    @bands = @q.result(distinct: true)
  end

  def notification
  end
end
