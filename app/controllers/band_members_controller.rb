require 'pry'
class BandMembersController < ApplicationController
  before_action :check_user_logged_in, only: [:edit, :new, :create, :invite]
  before_action only: [:edit, :delete, :destroy] do
    check_user_got_right(@band)
  end

  def edit
    find_band
    @band_member = @band.band_members.find(params[:id])
  end

  def invite
    @band = Band.where(link: params[:link_id]).first
    unless @band
      redirect_to root_path, flash: { error: 'Invalid invite link' }
      return
    end
    @band_member = @band.band_members.new
    unless current_user.nil? || @band.users.include?(current_user)
      @band_member.nickname = current_user.name
      render "new"
    end
  end

  def update
    find_band
    @band_member = BandMember.find(params[:id])
    @band_member.instruments = []
    instruments = params[:band_member][:instruments].select { |value| value != "" }
    another_instrument = params[:band_member][:another_instrument]
    if another_instrument == "" && instruments.empty?
      @instruments_empty = true
      render "edit"
      return
    else
      @instruments_empty = false
    end
    instruments.each do |instrument|
      @band_member.instruments << Instrument.find(instrument)
    end
    if @band_member.update(band_member_params)
      redirect_to @band
    else
      render 'edit', flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def delete
    find_band
    @band_member = @band.band_members.find(params[:id])
  end

  def destroy
    find_band
    @band.band_members.find(params[:id]).destroy
    redirect_to @band
  end

  def create
    find_band
    @band_member = BandMember.new(band_member_params)
    @band_member.instruments = []
    @band_member.band = @band
    params[:band_member][:instruments].each do |instrument|
      unless instrument == ""
        @band_member.instruments << Instrument.find(instrument)
      end
    end
    unless current_user.nil?
      @band_member.user = current_user
    end
    if @band_member.save
      redirect_to @band
    else
      render "new", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def band_member_params
    params.require(:band_member).permit(:nickname, :instruments, :another_instrument)
  end

  def find_band
    @band = Band.find(params[:band_id])
  end
end
