class EventsController < ApplicationController
  before_action :check_user_logged_in, only: [:edit, :new, :create, :update]
  before_action only: [:edit, :destroy, :delete, :create, :new, :update] do
    check_user_got_right(@band)
  end
  def show
    find_band
    @event = @band.events.find(params[:id])
    if @event.date < Date.yesterday
      @event.destroy
    end
  end

  def destroy
    find_band
    @event = @band.events.find(params[:id])
    @event.destroy
    redirect_to @band
  end

  def edit
    find_band
    @event = @band.events.find(params[:id])
  end

  def update
    find_band
    @event = @band.events.find(params[:id])
    event_type = EventType.find(params[:event][:event_type])
    @event.event_type = event_type
    if @event.update(event_params)
      redirect_to @band
    else
      render "edit", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def new
    find_band
    @event = Event.new
  end

  def create
    find_band
    @event = Event.new(event_params)
    @event.band = @band
    event_type = EventType.find(params[:event][:event_type])
    @event.event_type = event_type
    if @event.save
      redirect_to @band
    else
      render "new", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def event_params
    params.require(:event).permit(:text, :header, :date, :place, :lat, :lng)
  end

  def find_band
    @band = Band.find(params[:band_id])
  end
end
