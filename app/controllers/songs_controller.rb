class SongsController < ApplicationController
  before_action :check_user_logged_in, only: [:edit, :new, :create, :update, :destroy]
  before_action only: [:edit, :create, :new, :update, :destroy] do
    check_user_got_right(@band)
  end
  def new
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:album_id])
    @song = @album.songs.new
  end

  def create
    set_name
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:album_id])
    @song = @album.songs.new(song_params)
    @song.priority = @album.songs.length
    if @song.save
      redirect_to @band
    else
      render "new", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def song_params
    params.require(:song).permit(:name, :file)
  end

  def edit
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:album_id])
    @song = @album.songs.find(params[:id])
  end

  def destroy
    @band = Band.find(params[:band_id])
    @song = Song.find(params[:id])
    @song.destroy
    redirect_to @band
  end

  def update
    set_name
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:album_id])
    @song = @album.songs.find(params[:id])
    if @song.update(song_params)
      redirect_to @band
    else
      render "edit", flash: { notice: 'An error occurred, please try it again' }
    end
  end

  def set_name
    if params[:song][:name] == ""
      params[:song][:name] = params[:song][:file].original_filename
    end
  end
end
