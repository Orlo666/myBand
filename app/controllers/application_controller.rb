class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_global_search_variable

  def set_global_search_variable
    @q = Band.joins(:albums, :band_members).ransack(params[:q])
  end

  class NotLoggedIn < StandardError
  end

  class DontHavePermission < StandardError
  end

  rescue_from NotLoggedIn, :with => :not_logged_in
  rescue_from DontHavePermission, :with => :dont_have_permission

  def not_logged_in
    flash[:notice] = "User is not logged."
    render 'errors/log_in'
  end

  def dont_have_permission
    flash[:notice] = "You dont have permission."
    render 'errors/permission'
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  helper_method :current_user

  def check_user_logged_in
    raise NotLoggedIn unless current_user
  end

  def check_user_got_right(band)
    if params[:band_id]
      band = Band.find(params[:band_id])
    elsif params[:id]
      band = Band.find(params[:id])
    end
    raise DontHavePermission unless band.users.include?(current_user)
  end
end
