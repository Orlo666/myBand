# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require html.sortable
$(document).on "turbolinks:load", ->
  if $('#sortable').length
    sortable($('#sortable'), {
      placeholder: "<div style='border-style: dashed'>&nbsp;&nbsp;&nbsp;</div>"
      }
    )[0].addEventListener('sortupdate', (e, ui) ->
      orders = []
      e.detail.endparent.childNodes.forEach((e) -> 
        orders.push(e.id) 
      )
      document.getElementById('items_ids').value = JSON.stringify(orders)
    )
  

