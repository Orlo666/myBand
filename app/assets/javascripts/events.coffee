# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
window.MyApp ||= {}
class MyApp.Events extends MyApp.Base
  constructor:() ->
    super  # call Base class for core functionality
    this   # and be sure to return this

  # EDIT
  edit:() ->
    event_lat = $('#event_lat')[0]
    event_lng = $('#event_lng')[0]
    marker = null

    handler = Gmaps.build('Google')
    myLatlng = {lat: parseFloat(event_lat.value), lng: parseFloat(event_lng.value)}
    handler.buildMap {
      provider: {zoom: 4}
      internal: id: 'map'
    }, ->
      handler.map.centerOn(myLatlng)
      marker = handler.addMarker({
        'lat': parseFloat(event_lat.value),
        'lng': parseFloat(event_lng.value),
        'picture':
            'width': 32
            'height': 32
      })
      return

    map = handler.getMap()

    map.addListener 'click', (e)->
      if marker
        handler.removeMarker(marker)
      marker = handler.addMarker({
          'lat': e.latLng.lat(),
          'lng': e.latLng.lng(),
          'picture':
              'width': 32
              'height': 32
      })
      url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+e.latLng.lat()+","+e.latLng.lng()+"&language=en"
      $.ajax(url: url).done (json) ->
        event_place = $('#event_place')
        if event_place.length > 0
            if json.results.length > 0
              for result in json.results
                if result.types.indexOf('street_address') > -1
                  street_address = result.formatted_address
                  break;
                else if result.types.indexOf('locality') > -1
                  locality = result.formatted_address
              if street_address
                event_place[0].value = street_address
              else if locality
                event_place[0].value = locality
              else
                event_place[0].value = ""
            else
              event_place[0].value = ""
      return

  # NEW
  new:() ->
    marker = null
    handler = Gmaps.build('Google')
    myLatlng = {lat: -25.363, lng: 131.044}
    handler.buildMap {
      center: myLatlng
      provider: {zoom: 4}
      internal: id: 'map'
    }, ->
      handler.map.centerOn({ lat: 40, lng: 5 })
      return

    map = handler.getMap()

    map.addListener 'click', (e)->
      # 3 seconds after the center of the map has changed, pan back to the
      # marker.
      if marker
        handler.removeMarker(marker)
      marker = handler.addMarker({
          'lat': e.latLng.lat(),
          'lng': e.latLng.lng(),
          'picture':
              'width': 32
              'height': 32
      })
      $('#event_lat')[0].value = e.latLng.lat();
      $('#event_lng')[0].value = e.latLng.lng();
      url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+e.latLng.lat()+","+e.latLng.lng()+"&language=en"
      $.ajax(url: url).done (json) ->
        event_place = $('#event_place')
        if event_place.length > 0
            if json.results.length > 0
              for result in json.results
                if result.types.indexOf('street_address') > -1
                  street_address = result.formatted_address
                  break;
                else if result.types.indexOf('locality') > -1
                  locality = result.formatted_address
              if street_address
                event_place[0].value = street_address
              else if locality
                event_place[0].value = locality
              else
                event_place[0].value = ""
            else
              event_place[0].value = ""
      return

  # SHOW
  show:() ->
    a = "<%= @event %>"
    event_lat = $('#event_lat')[0]
    event_lng = $('#event_lng')[0]
    marker = null

    handler = Gmaps.build('Google')
    myLatlng = {lat: parseFloat(event_lat.value), lng: parseFloat(event_lng.value)}
    handler.buildMap {
      provider: {zoom: 4}
      internal: id: 'map'
    }, ->
      handler.map.centerOn(myLatlng)
      marker = handler.addMarker({
        'lat': parseFloat(event_lat.value),
        'lng': parseFloat(event_lng.value),
        'picture':
            'width': 32
            'height': 32
      })
      return
