# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
$(document).on "turbolinks:load", ->
  audiojs.events.ready ->
    a = audiojs.createAll(trackEnded: ->
      next = $('ol li.playing').next()
      if !next.length
        next = $('ol li').first()
      next.addClass('playing').siblings().removeClass 'playing'
      audio.load $('a', next).attr('data-src')
      audio.play()
      return
    )
    # Load in the first track
    if a.length > 0
      audio = a[0]
      first = $('ol a').attr('data-src')
      $('ol li').first().addClass 'playing'
      audio.load first
    # Load in a track on click
    $('.play-song').click (e) ->
      e.preventDefault()
      $(this).parent().addClass('playing').siblings().removeClass 'playing'
      audio.load $('a', this.parentElement).attr('data-src')
      audio.play()
    return
  $.fn.stars = ->
    $(this).each ->
      # Get the value
      val = parseFloat($(this).html())
      val = Math.round(val * 4) / 4;
      # Make sure that the value is in 0 - 5 range, multiply to get width
      size = Math.max(0, Math.min(7, val)) * 15
      # Create stars holder
      $span = $('<span />').width(size)
      # Replace the numerical value with stars
      $(this).html $span
      return
  $('span.stars').stars();
