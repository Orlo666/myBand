# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->
  $.fn.stars = ->
    $(this).each ->
      # Get the value
      val = parseFloat($(this).html())
      val = Math.round(val * 4) / 4;
      # Make sure that the value is in 0 - 5 range, multiply to get width
      size = Math.max(0, Math.min(7, val)) * 15
      # Create stars holder
      $span = $('<span />').width(size)
      # Replace the numerical value with stars
      $(this).html $span
      return
  $('span.stars').stars();
