class Album < ApplicationRecord
  has_many :songs
  belongs_to :band
  has_many :user_review
  mount_uploader :pict, AttachmentUploader
  validates :name, presence: true
end
