class Event < ApplicationRecord
  belongs_to :event_type
  belongs_to :band
  validates :header, presence: true
  validates :text, presence: true
end
