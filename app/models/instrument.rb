class Instrument < ApplicationRecord
  has_and_belongs_to_many :band_members
  validates :name, presence: true
end
