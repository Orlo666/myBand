class BandMember < ApplicationRecord
  belongs_to :user
  belongs_to :band
  has_and_belongs_to_many :instruments
  validates :nickname, presence: true
  validates :instruments, presence: true, unless: ->(band_member) { band_member.another_instrument.present? }
  validates :another_instrument, presence: true, unless: ->(band_member) { band_member.instruments.present? }
end
