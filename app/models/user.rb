class User < ApplicationRecord
  has_many :band_members
  has_many :bands, through: :band_members
  has_many :user_reviews
  validates :name, presence: true
  mount_uploader :avatar, AttachmentUploader
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.first_name if user.name.nil? || user.name.empty?
      user.email = auth.info.email
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end
end
