class Song < ApplicationRecord
  belongs_to :album
  mount_uploader :file, AttachmentUploader
  validates :name, presence: true
  validates :file, presence: true
  validates :priority, presence: true
end
