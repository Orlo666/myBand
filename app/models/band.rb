class Band < ApplicationRecord
  has_many :albums
  has_many :band_members
  has_many :users, through: :band_members
  has_many :user_reviews
  has_many :events
  mount_uploader :logo, AttachmentUploader
  validates :name, presence: true
end
