class UserReview < ApplicationRecord
  belongs_to :band
  belongs_to :user, optional: true
  validates :review, presence: true
  validates :rating, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than: 8 }
end
