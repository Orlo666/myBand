# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170205143347) do

  create_table "albums", force: :cascade do |t|
    t.string   "name"
    t.string   "pict"
    t.integer  "band_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["band_id"], name: "index_albums_on_band_id"
  end

  create_table "band_members", force: :cascade do |t|
    t.string   "nickname"
    t.integer  "band_id"
    t.integer  "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.text     "another_instrument"
    t.index ["band_id"], name: "index_band_members_on_band_id"
    t.index ["user_id"], name: "index_band_members_on_user_id"
  end

  create_table "band_members_instruments", id: false, force: :cascade do |t|
    t.integer "instrument_id"
    t.integer "band_member_id"
    t.index ["band_member_id"], name: "index_band_members_instruments_on_band_member_id"
    t.index ["instrument_id"], name: "index_band_members_instruments_on_instrument_id"
  end

  create_table "bands", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "logo"
    t.float    "rating"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "link"
  end

  create_table "event_types", force: :cascade do |t|
    t.text     "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.integer  "event_type_id"
    t.string   "header"
    t.string   "text"
    t.integer  "band_id"
    t.datetime "date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "place"
    t.float    "lat"
    t.float    "lng"
    t.index ["band_id"], name: "index_events_on_band_id"
    t.index ["event_type_id"], name: "index_events_on_event_type_id"
  end

  create_table "instruments", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "songs", force: :cascade do |t|
    t.string   "name"
    t.integer  "album_id"
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "priority"
    t.index ["album_id"], name: "index_songs_on_album_id"
  end

  create_table "user_reviews", force: :cascade do |t|
    t.text     "review"
    t.integer  "rating"
    t.integer  "band_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["band_id"], name: "index_user_reviews_on_band_id"
    t.index ["user_id"], name: "index_user_reviews_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "email"
    t.string   "description"
    t.string   "avatar"
  end

end
