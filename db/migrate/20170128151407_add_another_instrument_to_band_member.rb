class AddAnotherInstrumentToBandMember < ActiveRecord::Migration[5.0]
  def change
    add_column :band_members, :another_instrument, :text
  end
end
