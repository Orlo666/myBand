class CreateAlbums < ActiveRecord::Migration[5.0]
  def change
    create_table :albums do |t|
      t.string :name
      t.string :pict
      t.belongs_to :band

      t.timestamps
    end
  end
end
