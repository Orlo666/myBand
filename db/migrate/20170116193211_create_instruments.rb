class CreateInstruments < ActiveRecord::Migration[5.0]
  def change
    create_table :instruments do |t|
      t.string :name

      t.timestamps
    end
    
    create_table :band_members_instruments, id: false do |t|
      t.belongs_to :instrument, index: true
      t.belongs_to :band_member, index: true
    end
  end
end
