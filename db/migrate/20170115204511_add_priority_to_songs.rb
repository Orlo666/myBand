class AddPriorityToSongs < ActiveRecord::Migration[5.0]
  def change
    add_column :songs, :priority, :integer
  end
end
