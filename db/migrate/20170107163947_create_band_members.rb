class CreateBandMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :band_members do |t|
      t.string :nickname
      t.belongs_to :band, index: true
      t.belongs_to :user, index: true
      t.timestamps
    end
  end
end
