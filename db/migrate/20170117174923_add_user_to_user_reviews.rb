class AddUserToUserReviews < ActiveRecord::Migration[5.0]
  def change
    add_reference :user_reviews, :user, foreign_key: true
  end
end
