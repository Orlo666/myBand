class AddLinkToBands < ActiveRecord::Migration[5.0]
  def change
    add_column :bands, :link, :string
  end
end
