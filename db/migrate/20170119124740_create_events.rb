class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.belongs_to :event_type, foreign_key: true
      t.string :header
      t.string :text
      t.belongs_to :band, foreign_key: true
      t.datetime :date

      t.timestamps
    end
  end
end
