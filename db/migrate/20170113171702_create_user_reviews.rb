class CreateUserReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :user_reviews do |t|
      t.text :review
      t.integer :rating
      t.belongs_to :band

      t.timestamps
    end
  end
end
